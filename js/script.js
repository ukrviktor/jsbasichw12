// Відповіді на питання:

// 1.Чому для роботи з input не рекомендується використовувати клавіатуру?
// Якщо ми хочемо відстежувати будь які події в полі inpu то використовувати лише клавіатуру не рекомендується з наступних причин:
// •	можливо введення в поле input за допомогою розпізнавання мови;
// •	можливо введення в поле input за допомогою миші ( копіювати/вставити)


// Завдання №1

const listKey = document.querySelector('.btn-wrapper');

function handleKey(e) {
    let elemForKey = findElementForKey(e.key.toUpperCase());
    if (!elemForKey) return;
    unpaintAllKey();
    paintKey(elemForKey);
}

function findElementForKey(keyValue) {
    return [...listKey.children].find(elem => elem.textContent.toUpperCase() === keyValue);
}

function paintKey(elemForKey) {
    elemForKey.style.backgroundColor = 'blue';
}

function unpaintAllKey() {
    [...listKey.children].forEach((elem) => {
        elem.removeAttribute('style');
    });
}

document.addEventListener('keydown', handleKey);